/// <reference types="node" />
import { URL } from "url";
export { isNumber, arrayUnique, cleanDomain, map, filter, forEach, trim, isDomain, find, removeFromArray, forceUrl, getEnvironmentVariables };
/**
 * @description filters `array` when `fn()` returns true
 * @param {Array} array
 * @param {Function} fn callback function
 */
declare function filter(array: any[], fn: (item: any) => boolean): any[];
/**
 * @description loops through an array and passes each item to the `fn` function
 * @param {Array} array
 * @param {Function} fn `fn(value, index, array)`
 */
declare function forEach(array: any[], fn: (...args: any[]) => any): void;
/**
 * @description loops through array and returns the first matching item or `false`
 * @param {Array} array
 * @param {Function} condition
 */
declare function find(array: any[], condition: (...args: any[]) => boolean): any;
/**
 *
 * @param {Array} array
 * @param {String|Number} item item to remove from array
 * @returns {Boolean}
 */
declare function removeFromArray(array: any[], item: string | number): boolean;
/**
 * @description map function
 * @param {Array} array
 * @param {Function} fn function to run on each item of the array
 */
declare function map(array: any[], fn: (item: any) => any): any[];
/**
 *
 * @description Removes whitespace from the start and end of a string, and ensures it has only valid characters
 * @param {String} domain
 * @returns {String} cleaned domain
 */
declare function cleanDomain(domain: string): string;
/**
 * @description removes whitespace and tabs from start and end of `string`
 * @param {String} string
 * @returns {String}
 */
declare function trim(string: string): string;
/**
 * @description returns true if `domain` is a valid domain
 * @param {String} domain hostname
 * @returns {Boolean}
 */
declare function isDomain(domain: string): boolean;
/**
 * @description returns the unique items in the provided array
 * @param {Array} array
 * @returns {Array} unique array
 */
declare function arrayUnique(array: any[]): any[];
/**
 * @description Checks if `input` is a number, optionally between `min` and `max`
 *
 * @param {any} input
 * @param {Number} min
 * @param {Number} max
 * @returns {Boolean}
 */
declare function isNumber(input: any, min?: number, max?: number): boolean;
/**
 * @description turns a url or partial url into URL object, or throws if it is invalid
 * @param {String} url
 * @param {Boolean} [https = false] use https as protocol (`false` / http by default)
 * @returns {URL} url
 */
declare function forceUrl(url: string, https?: boolean): URL;
interface IenvironmentVariableResults {
    [key: string]: any;
}
/**
 * @example const {env1, env2} = getEnvironmentVariables(['env1','env2']);
 * @param {Array} variables
 * @returns {Object} variables
 */
declare function getEnvironmentVariables(variables: string[]): IenvironmentVariableResults;
