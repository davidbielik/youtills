//@ts-check
const chai = require('chai');
const expect = chai.expect;

const utils = require('.');

describe('utils', () => {
    it('should export an object of functions', () => {
        expect(utils).to.be.an('object');
        utils.forEach(Object.keys(utils), util => {
            expect(utils[util]).to.be.a('function');
        });
    });
});



describe('utils.forceUrl', () => {
    it('should convert a hostname', () => {
        expect(utils.forceUrl('hostname.com').href).to.equal('http://hostname.com/');
    });

    it('should convert protocol', () => {
        expect(utils.forceUrl('http://hostname.com').href).to.equal('http://hostname.com/');
    });

    it('should convert HTTPS protocol', () => {
        expect(utils.forceUrl('https://hostname.com').href).to.equal('https://hostname.com/');
    });

    it('should convert relative protocol', () => {
        expect(utils.forceUrl('//hostname.com').href).to.equal('http://hostname.com/');
    });

    it('should force HTTPS with relative protocol', () => {
        expect(utils.forceUrl('//hostname.com', true).href).to.equal('https://hostname.com/');
    });

    it('should not force HTTPS with HTTP protocol', () => {
        expect(utils.forceUrl('http://hostname.com', true).href).to.equal('http://hostname.com/');
    });

    it('should throw with an invalid hostname', () => {
        expect(function () {
            utils.forceUrl('hostnamecom');
        }).to.throw('Invalid URL');
    });

    it('should throw with null', () => {
        expect(function () {
            utils.forceUrl(null);
        }).to.throw('Invalid URL');
    });

    it('should throw with non-string', () => {
        expect(function () {
            // @ts-ignore
            utils.forceUrl({});
        }).to.throw('Invalid URL');
    });

    it('should not throw with localhost URLs', () => {
        expect(utils.forceUrl('http://localhost').href).to.equal('http://localhost/');
        expect(utils.forceUrl('http://localhost/path').href).to.equal('http://localhost/path');
        expect(utils.forceUrl('localhost').href).to.equal('http://localhost/');
        expect(utils.forceUrl('localhost', true).href).to.equal('https://localhost/');
        expect(utils.forceUrl('http://localhost/?asdf').href).to.equal('http://localhost/?asdf');
        expect(utils.forceUrl('http://localhost?asdf').href).to.equal('http://localhost/?asdf');
        expect(utils.forceUrl('localhost?asdf').href).to.equal('http://localhost/?asdf');
        expect(utils.forceUrl('localhost/asdf').href).to.equal('http://localhost/asdf');
    });
});

describe('utils.getEnvironmentVariables', () => {
    it('should return an object with the requested keys', () => {
        process.env.key1 = 'value1';
        process.env.key2 = 'value2';
        var { key1, key2 } = utils.getEnvironmentVariables(['key1', 'key2']);
        expect(key1).to.equal(process.env.key1);
        expect(key2).to.equal(process.env.key2);
        delete process.env.key1;
        delete process.env.key2;
    });

    it('should throw an exception when a key is not found', () => {
        delete process.env.key3;
        expect(function () {
            var { key3 } = utils.getEnvironmentVariables(['key3']);
            if (key3) {
                throw new Error('should not have key3');
            }
        }).to.throw('Missing "key3" environment variable');
    });
});
