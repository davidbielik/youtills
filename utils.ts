//@ts-check
import { URL } from "url";
export {
    isNumber,
    arrayUnique,
    cleanDomain,
    map,
    filter,
    forEach,
    trim,
    isDomain,
    find,
    removeFromArray,
    forceUrl,
    getEnvironmentVariables
}


/**
 * @description filters `array` when `fn()` returns true
 * @param {Array} array
 * @param {Function} fn callback function
 */
function filter(array: any[], fn: (item: any) => boolean) {
    var results = [];
    for (var i = 0, l = (array.length || []); i < l; i++) {
        if (fn(array[i])) {
            results.push(array[i]);
        }
    }
    return results;
}

/**
 * @description loops through an array and passes each item to the `fn` function
 * @param {Array} array 
 * @param {Function} fn `fn(value, index, array)`
 */
function forEach(array: any[], fn: (...args: any[]) => any) {
    for (var i = 0, l = (array || []).length; i < l; i++) {
        fn(array[i], i, array);
    }
}

/**
 * @description loops through array and returns the first matching item or `false`
 * @param {Array} array 
 * @param {Function} condition 
 */
function find(array: any[], condition: (...args: any[]) => boolean) {
    for (var i = 0, l = (array || []).length; i < l; i++) {
        if (condition(array[i], i, array)) {
            return array[i];
        }
    }
    return false;
}


/**
 * 
 * @param {Array} array 
 * @param {String|Number} item item to remove from array
 * @returns {Boolean}
 */
function removeFromArray(array: any[], item: string | number): boolean {
    var index = array.indexOf(item);
    if (index === -1) {
        return false;
    }
    array.splice(index, 1);
    return true;
}

/**
 * @description map function
 * @param {Array} array 
 * @param {Function} fn function to run on each item of the array
 */
function map(array: any[], fn: (item: any) => any): any[] {
    const results: any[] = [];
    forEach(array, item => {
        let result = fn(item);
        results.push(result);
    });
    return results;
}

/**
 * 
 * @description Removes whitespace from the start and end of a string, and ensures it has only valid characters
 * @param {String} domain 
 * @returns {String} cleaned domain
 */
function cleanDomain(domain: string): string {
    var cleaned = trim(domain || '');
    if (!isDomain(cleaned)) {
        throw new Error('Invalid domain: ' + cleaned);
    }
    return cleaned;
}

/**
 * @description removes whitespace and tabs from start and end of `string`
 * @param {String} string 
 * @returns {String}
 */
function trim(string: string): string {
    return (string || '').replace(/^[\s\t]*|[\s\t]*$/g, '');
}

/**
 * @description returns true if `domain` is a valid domain
 * @param {String} domain hostname
 * @returns {Boolean}
 */
function isDomain(domain: string): boolean {
    if (typeof domain !== 'string') {
        return false;
    }
    domain = trim(domain);
    if (!domain) {
        return false;
    }

    // should also support weird asian domains
    if (!/^[a-z0-9][a-z0-9_\-.]*\.[a-z0-9-]+$/i.test(domain)) {
        return false;
    }
    return true;
}

/**
 * @description returns the unique items in the provided array
 * @param {Array} array 
 * @returns {Array} unique array
 */
function arrayUnique(array: any[]): any[] {
    var unique = [];
    for (var i = 0, l = (array || []).length; i < l; i++) {
        if (unique.indexOf(array[i]) === -1) {
            unique.push(array[i]);
        }
    }
    return unique;
}



/**
 * @description Checks if `input` is a number, optionally between `min` and `max`
 * 
 * @param {any} input 
 * @param {Number} min 
 * @param {Number} max 
 * @returns {Boolean}
 */
function isNumber(input: any, min?: number, max?: number): boolean {
    var number = +(input || '');
    if (isNaN(number)) {
        return false;
    }
    if (!isFinite(number)) {
        return false;
    }

    if (number !== 0 && Number(number) !== number || number % 1 !== 0) {
        return false;
    }

    if (typeof min !== 'undefined' && number < min) {
        return false;
    }

    if (typeof max !== 'undefined' && number > max) {
        return false;
    }
    return true;
}

/**
 * @description turns a url or partial url into URL object, or throws if it is invalid
 * @param {String} url 
 * @param {Boolean} [https = false] use https as protocol (`false` / http by default)
 * @returns {URL} url
 */
function forceUrl(url: string, https: boolean = false): URL {
    const protocol = https ? 'https' : 'http';
    if (!url || typeof url !== 'string') {
        throw new Error('Invalid URL');
    }
    if (!/(^https?:\/\/)?localhost(\/.+)?/.test(url) && url.indexOf('.') === -1) {
        throw new Error('Invalid URL');
    }
    url = trim(url);
    if (/^https?:\/\//.test(url)) {
        return new URL(url);
    }
    return new URL(`${protocol}://${url.replace(/^(https?:)?\/\//, '')}`);
}

interface IenvironmentVariableResults {
    [key: string]: string;
}

/**
 * @example const {env1, env2} = getEnvironmentVariables(['env1','env2']);
 * @param {Array} variables
 * @returns {Object} variables
 */
function getEnvironmentVariables(variables: string[]): IenvironmentVariableResults {
    const object: IenvironmentVariableResults = {};
    variables.forEach(key => {
        var value = process.env[key];
        if (!value) {
            throw new Error(`Missing "${key}" environment variable`);
        }
        object[key] = value;
    });
    return object;
}
