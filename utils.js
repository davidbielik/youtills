"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//@ts-check
var url_1 = require("url");
/**
 * @description filters `array` when `fn()` returns true
 * @param {Array} array
 * @param {Function} fn callback function
 */
function filter(array, fn) {
    var results = [];
    for (var i = 0, l = (array.length || []); i < l; i++) {
        if (fn(array[i])) {
            results.push(array[i]);
        }
    }
    return results;
}
exports.filter = filter;
/**
 * @description loops through an array and passes each item to the `fn` function
 * @param {Array} array
 * @param {Function} fn `fn(value, index, array)`
 */
function forEach(array, fn) {
    for (var i = 0, l = (array || []).length; i < l; i++) {
        fn(array[i], i, array);
    }
}
exports.forEach = forEach;
/**
 * @description loops through array and returns the first matching item or `false`
 * @param {Array} array
 * @param {Function} condition
 */
function find(array, condition) {
    for (var i = 0, l = (array || []).length; i < l; i++) {
        if (condition(array[i], i, array)) {
            return array[i];
        }
    }
    return false;
}
exports.find = find;
/**
 *
 * @param {Array} array
 * @param {String|Number} item item to remove from array
 * @returns {Boolean}
 */
function removeFromArray(array, item) {
    var index = array.indexOf(item);
    if (index === -1) {
        return false;
    }
    array.splice(index, 1);
    return true;
}
exports.removeFromArray = removeFromArray;
/**
 * @description map function
 * @param {Array} array
 * @param {Function} fn function to run on each item of the array
 */
function map(array, fn) {
    var results = [];
    forEach(array, function (item) {
        var result = fn(item);
        results.push(result);
    });
    return results;
}
exports.map = map;
/**
 *
 * @description Removes whitespace from the start and end of a string, and ensures it has only valid characters
 * @param {String} domain
 * @returns {String} cleaned domain
 */
function cleanDomain(domain) {
    var cleaned = trim(domain || '');
    if (!isDomain(cleaned)) {
        throw new Error('Invalid domain: ' + cleaned);
    }
    return cleaned;
}
exports.cleanDomain = cleanDomain;
/**
 * @description removes whitespace and tabs from start and end of `string`
 * @param {String} string
 * @returns {String}
 */
function trim(string) {
    return (string || '').replace(/^[\s\t]*|[\s\t]*$/g, '');
}
exports.trim = trim;
/**
 * @description returns true if `domain` is a valid domain
 * @param {String} domain hostname
 * @returns {Boolean}
 */
function isDomain(domain) {
    if (typeof domain !== 'string') {
        return false;
    }
    domain = trim(domain);
    if (!domain) {
        return false;
    }
    // should also support weird asian domains
    if (!/^[a-z0-9][a-z0-9_\-.]*\.[a-z0-9-]+$/i.test(domain)) {
        return false;
    }
    return true;
}
exports.isDomain = isDomain;
/**
 * @description returns the unique items in the provided array
 * @param {Array} array
 * @returns {Array} unique array
 */
function arrayUnique(array) {
    var unique = [];
    for (var i = 0, l = (array || []).length; i < l; i++) {
        if (unique.indexOf(array[i]) === -1) {
            unique.push(array[i]);
        }
    }
    return unique;
}
exports.arrayUnique = arrayUnique;
/**
 * @description Checks if `input` is a number, optionally between `min` and `max`
 *
 * @param {any} input
 * @param {Number} min
 * @param {Number} max
 * @returns {Boolean}
 */
function isNumber(input, min, max) {
    var number = +(input || '');
    if (isNaN(number)) {
        return false;
    }
    if (!isFinite(number)) {
        return false;
    }
    if (number !== 0 && Number(number) !== number || number % 1 !== 0) {
        return false;
    }
    if (typeof min !== 'undefined' && number < min) {
        return false;
    }
    if (typeof max !== 'undefined' && number > max) {
        return false;
    }
    return true;
}
exports.isNumber = isNumber;
/**
 * @description turns a url or partial url into URL object, or throws if it is invalid
 * @param {String} url
 * @param {Boolean} [https = false] use https as protocol (`false` / http by default)
 * @returns {URL} url
 */
function forceUrl(url, https) {
    if (https === void 0) { https = false; }
    var protocol = https ? 'https' : 'http';
    if (!url || typeof url !== 'string') {
        throw new Error('Invalid URL');
    }
    if (!/(^https?:\/\/)?localhost(\/.+)?/.test(url) && url.indexOf('.') === -1) {
        throw new Error('Invalid URL');
    }
    url = trim(url);
    if (/^https?:\/\//.test(url)) {
        return new url_1.URL(url);
    }
    return new url_1.URL(protocol + "://" + url.replace(/^(https?:)?\/\//, ''));
}
exports.forceUrl = forceUrl;
/**
 * @example const {env1, env2} = getEnvironmentVariables(['env1','env2']);
 * @param {Array} variables
 * @returns {Object} variables
 */
function getEnvironmentVariables(variables) {
    var object = {};
    variables.forEach(function (key) {
        var value = process.env[key];
        if (!value) {
            throw new Error("Missing \"" + key + "\" environment variable");
        }
        object[key] = value;
    });
    return object;
}
exports.getEnvironmentVariables = getEnvironmentVariables;
